<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="post">
        @csrf
        <label for="firstname">First Name:</label> <br> <br>
        <input type="text" name="firstname" id="firstname"> <br> <br>
        <label for="lastname">Last Name:</label> <br> <br>
        <input type="text"name="lastname" id="lastname"> <br> <br>
        <label for="">Gender: </label> <br> <br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Orther <br> <br>
        <label>Nationality: </label> <br> <br>
        <select name="Nationality">
            <option value="Indonesia">Indonesia  </option>
            <option value="Malaysia">Malaysia  </option>
            <option value="Singapura">Singapura  </option>
            <option value="Australia">Australia  </option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Orther <br> <br>
        <label>Bio: </label> <br> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>